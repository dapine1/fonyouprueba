package com.fonyou.studens.studens.infraestructure.ports.input.repository;

import com.fonyou.studens.studens.domain.model.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {
}