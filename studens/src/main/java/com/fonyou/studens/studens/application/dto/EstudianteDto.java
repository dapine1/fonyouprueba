package com.fonyou.studens.studens.application.dto;

import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.io.Serializable;

/**
 * DTO for {@link com.fonyou.studens.studens.domain.model.Estudiante}
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class EstudianteDto implements Serializable {
    private Long id;
    @NotNull
    private String nombre;
    @NotNull
    private Integer edad;
    @NotNull
    private String ciudad;
    @NotNull
    private String zonaHoraria;
}